function MathOperation () {
let needToCalculate = true;

while (needToCalculate) {
  let numInput1;
  do {
    numInput1 = prompt('Enter number 1:');
  } while (numInput1 === null || numInput1.trim() === '' || isNaN(+numInput1));

  let operation;
  do {
    operation = prompt('Enter operation (+ - * /):');
  } while (operation !== '+'
  && operation !== '-'
  && operation !== '/'
  && operation !== '*');
  let numInput2;
  do {
    numInput2 = prompt('Enter number 2:');
  } while (numInput2 === null || numInput2.trim() === '' || isNaN(+numInput2));

  let result;

  switch (operation) {
    case '+':
      result = +numInput1 + +numInput2;
      break;
    case '-':
      result = +numInput1 - +numInput2;
      break;
    case '*':
      result = +numInput1 * +numInput2;
      break;
    case '/':
      result = +numInput1 / +numInput2;
      break;
  }

  alert(result);

  needToCalculate = confirm('Once more?');
}
}
MathOperation ()