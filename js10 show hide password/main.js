// ПОЛУЧАЮ ЕЛЕМЕНТЫ 
let yeasOne = document.getElementById('n-icon'); // картинка первого инпута 
let yeasTwo = document.getElementById('f-icon'); // картинка второго инпута 
let inputOne = document.getElementById('password-inputOne');// инпут первый 
let inputTwo = document.getElementById('password-inputTwo'); // инпут второй
let Errorspan = document.createElement('section'); // сообщение об ошибке
let lableTwo = document.getElementById('lableTwo'); // второй лейбл
let button = document.getElementsByClassName('btn'); // кнопка
let form = document.getElementById('form'); // форма

//  СОЗДАЮ ОБРАБОТЧИК СОБЫТИЙ НА ФОРМУ ЧТО БЫ СЛУШАТЬ САБМИТ И ОТМЕНИТЬ ЕГО 
form.addEventListener("submit", preventEvent);


function show_hide_passwordIconOne(target){
	let input = document.getElementById('password-inputOne'); // получаю первый инпут 
	if (input.getAttribute('type') === 'password') { // сравниваю с типом пароля 
        yeasOne.setAttribute('class', 'fas fa-eye icon-password');// если true меняю иконку 
		input.setAttribute('type', 'text');// меняю тип атрибута инпута
	} else {
        input.setAttribute('type', 'password');
        yeasOne.setAttribute('class', 'fas fa-eye-slash icon-password');
        // здесь все наоборот 
	}
    return false;
    

}


//  тоже самое что и с первым инпутом
function show_hide_passwordIconTwo(target){
	let input = document.getElementById('password-inputTwo');
	if (input.getAttribute('type') === 'password') {
        yeasTwo.setAttribute('class', 'fas fa-eye icon-password');
		input.setAttribute('type', 'text');
	} else {
        input.setAttribute('type', 'password');
        yeasTwo.setAttribute('class', 'fas fa-eye-slash icon-password');
        
    }
    return false;
}

function submited(target){

  
    if (inputTwo.value !== inputOne.value && inputOne.value.trim() === '' && inputTwo.value.trim() === '') {
        errorText();
    }
    if (inputOne.value === inputTwo.value && inputOne.value.trim() !== '' && inputTwo.value.trim() !== '') { // проверяю введенные значения двух инпутов ..
        alert('You are welcome')
    }
    else errorText();
        // добавляю текст с ошибкой ввода

}

function removetError(){
    Errorspan.remove(); // удаляю еррор при клике на инпут 
}

function preventEvent( event ) {
    if ( event.cancelable ) { //  если событие может быть отменено и предотвращено
      event.preventDefault(); // отменяем действие события по умолчанию
      console.log("Событие " + event.type + " отменено"); //  выводим в консоль информацию о том какое событие было отменено
    } else { //  если событие не может быть отменено и предотвращено
      console.warn("Событие " + event.type + " не может быть отменено"); //  выводим в консоль информацию о том, что данное событие не может быть отменено
    }
  }

  let errorText = function () {
    lableTwo.append(Errorspan); 
    Errorspan.innerHTML = 'Нужно ввести одинаковые значения';
    Errorspan.style = "color : red "
  }

