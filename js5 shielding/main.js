
let createNewUser = () => {
  let firstName, lastName, birthday;
  let enteredValue = firstValue => {
  return (!isNaN(firstValue) || firstValue === 'null');
};
let checkDate = date => {
  if (date !== null && isNaN(+date)) {
      const birthdayChecked = date.split('.');
      const d = new Date(birthdayChecked[1] + '.' + birthdayChecked[0] + '.' + birthdayChecked[2]);
      return !(d && (d.getMonth() + 1) == birthdayChecked[1] && d.getDate() == Number(birthdayChecked[0]) && d.getFullYear() == Number(birthdayChecked[2]));
  } else {
      return true
  }
};
  do {
      firstName = prompt('Enter first name', 'first name');
      lastName = prompt('Enter last name', 'last name');
  } while (enteredValue(firstName) || enteredValue(lastName));
  do {
      birthday = prompt('Enter your birth date', 'dd.mm.yyyy');
  } while (checkDate(birthday));
  return {
      firstName: firstName,
      lastName: lastName,
      getLogin: function () {
          return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
      },
      birthday: birthday,
      getAge: function () {
          const birthdayChecked = this.birthday.split('.');
          const birthdayDate = new Date(birthdayChecked[1] + '.' + birthdayChecked[0] + '.' + birthdayChecked[2]);
          let age = new Date().getFullYear() - birthdayDate.getFullYear();
          if (new Date().getMonth() < birthdayDate.getMonth() || (new Date().getMonth() === birthdayDate.getMonth() && new Date().getDate() < birthdayDate.getDate())) age--;
          return age;
      },
      getPassword: function () {
    return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
      },
  };
};
let user = createNewUser();
console.log(`Result of createAge() :${user.getAge()}`);
console.log(`Result of createNewUser() : ${user}`);
console.log(`Result of getPassword(): ${user.getPassword()}`);