const $pageNav = $('.Nav a');
const $htmbody = $('html, body');
const $slideToggleButton = $('.rated');
const $hotNews = $('.popular-h2, .news');
$slideToggleButton.after().on('click', function () {
    $hotNews.slideToggle(1100);
});
const $window = $(window);
$window.on('scroll', function () {
    let scrollVal = $window.height();
    if ($window.scrollTop() >= scrollVal) {
        $TopBtn.fadeIn();
    } else {
        $TopBtn.fadeOut();
    }
});
$pageNav.on('click', function (event) {
    if (this.hash !== "") {
        event.preventDefault();
        let hash = this.hash;
        $htmbody.animate({
            scrollTop: $(hash).offset().top
        }, 500, function () {
            window.location.hash = hash;
        });
    }
});
const $TopBtn = $('.TopBtn');
$TopBtn.on('click', function () {
    $htmbody.animate({
        scrollTop: 0
    }, 400);
});